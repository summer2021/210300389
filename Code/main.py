import numpy as np
import taos
import dask.dataframe as dd

from ReadCSV import DaskRead
from TDconnect import taos_db

key_list = ['date', 'case_number', 'arrest', 'x_coord', 'latitude']

# create DaskRead class
data = DaskRead("../Data/Crimes_-_2001_to_Present.csv")

# create taosdb class
taosdb = taos_db()
record = []
for i in range(data.data_size):
    init_data = data.get_data(i)
    key_index = data.find_index('Beat')
    if init_data[key_index] not in record:
        record.append(init_data[key_index])
        taosdb.add_table('Beat_'+init_data[key_index])
    
    data_list = []
    for j in key_list:
        key_num = data.find_index(j)
        data_list.append(init_data[key_num])
    taosdb.add_data('Beat_'+init_data[key_index],tuple(data_list))
    

