import taos

# taos object taosd 
class taos_db:
    conn = taos.connect(host="127.0.0.1", user="root", password="taosdata", config="/etc/taos")
    c = conn.cursor()
    # init object
    def __init__(self):
        # init super table
        try:
            self.c.execute("create database crimes;")
            self.c.execute("use crimes;")
        except:
            self.c.execute("use crimes;")
        init_command = 'CREATE STABLE crimes '+\
            '(date timestamp, case_number int, arrest bool, x_coord float, latitude float) '+\
                'TAGS (beat binary(64), block binary(64), district binary(64), ward int);'
        # super table
        self.c.execute(init_command)

    # add table function
    def add_table(self,table):
        add_command = "create table"+str(table)+"tags(beat binary(64), block binary(64), district binary(64), ward int);"
        self.c.execute(add_command)
        
    # add data function
    def add_data(self,table_name,data):
        add_command = "insert into "+str(table_name) +" value"+str(data)+";"
        self.c.execute(add_command)

