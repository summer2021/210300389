import dask.dataframe as dd
import numpy as np
class DaskRead:

    def __init__(self,file):
        self.df = dd.read_csv(file, dtype={'Community Area': 'float64',
       'IUCR': 'object',
       'Ward': 'float64'})
        self.data_size = 269211
        self.data = self.df.head(self.data_size).values

    def get_data(self,index):
        return self.data[index]

    def get_colunm(self):
        return self.df.columns

    def find_index(self,key):
        return list(self.df.columns).index(key)
