# 时序数据演示案例的 TDengine 移植（芝加哥犯罪事件）

**项目描述：**
time-series-demo（[https://github.com/mesosphere-backup/time-series-demo](https://github.com/mesosphere-backup/time-series-demo)）是基于芝加哥犯罪记录开放数据（[https://data.cityofchicago.org/Public-Safety/Crimes-2001-to-Present/ijzp-q8t2](https://data.cityofchicago.org/Public-Safety/Crimes-2001-to-Present/ijzp-q8t2)）而创建的时序数据处理系统的演示性开源项目。该项目的分析功能丰富、可视化呈现的界面设计生动有趣，但依赖的软件模块比较复杂，部署和使用也都比较困难。 本项目计划对这一演示系统进行移植和改造，以 TDengine 作为唯一的数据存储介质和计算引擎，简化系统整体结构，同时也作为时序数据库应用的一个样例供开发者参考。

## 使用

本项目是数据迁移项目，首先需要安装TDengine物联网数据库软件，可以去TDengine官网 [TDengine | 物联网大数据平台，时序大数据引擎 (taosdata.com)](https://www.taosdata.com/cn/) 进行下载和安装。

项目包含有三个文件夹

- Data
- Code
- Software

Data文件夹存放的是芝加哥犯罪的数据文件

Code文件夹存放的是迁移数据所使用到的代码文件

Software文件夹存放的是一些必要的软件

使用步骤：

_默认使用Linux系统进行部署_

1. 安装TDengine的Python连接器

   ```shell
   cd Software && tar -zxvf TDengine-server-2.2.0.2
   pip install TDengine-server-2.2.0.2/connector/python/
   ```

2. 使用代码进行迁移

   ```shell
   python Code/main.py
   ```

3. Grafana插件的部署

   ```python
    sudo cp -r Software/TDengine-server-2.2.0.2/connector/grafanaplugin/ /var/lib/grafana/plugins/tdengine/
   ```

4. 运行Grafana

   ```shell
   sudo systemctl status grafana-server
   ```

5. 配置Grafana

   编辑Grafana的配置文件，让Grafana可以使用grafana插件

   ```shell
   sudo vim /etc/grafana/grafana.ini
   ```

   修改成如下情况：

   ```ini
   [plugins]
   ;enable_alpha = true
   ;app_tls_skip_verify_insecure = false
   # Enter a comma-separated list of plugin identifiers to identify plugins to load even if they are unsigned. Plugins with modified signatures are never loaded.
   allow_loading_unsigned_plugins = taosdata-tdengine-datasource
   # Enable or disable installing plugins directly from within Grafana.
   ;plugin_admin_enabled = false
   ;plugin_admin_external_manage_enabled = false
   ;plugin_catalog_url = https://grafana.com/grafana/plugins/
   ```

   然后保存配置文件，就可以使用Grafana进行数据板观察

6. 使用Grafana

   访问 127.0.0.1:3000

   可以看到Grafana的登录页面，进行admin/admin登录后修改密码

   然后就可以进行配置：

   ![image-20210930200511062](README.assets/image-20210930200511062.png)

   点击Configuration>Datasource来添加数据源来添加TDengine的数据源

   ![image-20210930200820256](README.assets/image-20210930200820256.png)

   点开Datasource数据源入口，然后点击 Add Datasource

   ![image-20210930200956354](README.assets/image-20210930200956354.png)

   滚动到最下面可以找到TDengine数据源，然后添加

   ![image-20210930201047821](README.assets/image-20210930201047821.png)

   根据默认配置进行填写就可以了，创建数据源，下面就是设置看板等功能，完全可以自定义化进行相应的配置和设置
